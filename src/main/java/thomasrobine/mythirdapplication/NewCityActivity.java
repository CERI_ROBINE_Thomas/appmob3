package thomasrobine.mythirdapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;



public class NewCityActivity extends AppCompatActivity {

    private City city;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);

        final Button but = (Button) findViewById(R.id.button);
        assert but!=null;
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                city = new City();
                EditText ville = (EditText) findViewById(R.id.editNewName);
                EditText pays = (EditText) findViewById(R.id.editNewCountry);
                city.setName(ville.getText().toString());
                city.setCountry(pays.getText().toString());
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(),NewCityActivity.class);
        intent.putExtra("Ville_update",city);
        setResult(Activity.RESULT_OK,intent);
        super.onBackPressed();
    }


}
