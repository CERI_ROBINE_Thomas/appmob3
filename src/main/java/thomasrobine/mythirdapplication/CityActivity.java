package thomasrobine.mythirdapplication;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;



public class CityActivity extends AppCompatActivity {

    private static final String TAG = CityActivity.class.getSimpleName();
    private TextView textCityName, textCountry, textTemperature, textHumdity, textWind, textCloudiness, textLastUpdate;
    private ImageView imageWeatherCondition;
    private City city;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);
        Intent intent = getIntent();

        city = intent.getExtras().getParcelable("Ville_informations");

        textCityName = (TextView) findViewById(R.id.nameCity);
        textCountry = (TextView) findViewById(R.id.country);
        textTemperature = (TextView) findViewById(R.id.editTemperature);
        textHumdity = (TextView) findViewById(R.id.editHumidity);
        textWind = (TextView) findViewById(R.id.editWind);
        textCloudiness = (TextView) findViewById(R.id.editCloudiness);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);
        imageWeatherCondition = (ImageView) findViewById(R.id.imageView);

        updateView();

        final Button but = (Button) findViewById(R.id.button);

        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                UpdateCityTask uct = new UpdateCityTask(city);
                uct.execute();
            }
        });
    }

    private void updateView() {

        textCityName.setText(city.getName());
        textCountry.setText(city.getCountry());
        textTemperature.setText(city.getTemperature() + " °C");
        textHumdity.setText(city.getHumidity() + " %");
        textWind.setText(city.getFullWind());
        textCloudiness.setText(city.getHumidity() + " %");
        textLastUpdate.setText(city.getLastUpdate());

        if (city.getIcon() != null && !city.getIcon().isEmpty()) {
            imageWeatherCondition.setImageDrawable(getResources().getDrawable(getResources()
                    .getIdentifier("@drawable/" + "icon_" + city.getIcon(), null, getPackageName())));
        }

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(),CityActivity.class);
        intent.putExtra("Ville_update",city);
        setResult(Activity.RESULT_OK,intent);
        super.onBackPressed();
    }

    class UpdateCityTask extends AsyncTask<URL, Integer, Long> {

        private City city;

        public UpdateCityTask(City c) {
            city = c;
        }

        protected Long doInBackground(URL... urls) {
            try {
                URL url = WebServiceUrl.build(city.getName(), city.getCountry());
                HttpURLConnection u = (HttpURLConnection) url.openConnection();
                InputStream stream = u.getInputStream();
                u.connect();
                JSONResponseHandler reader = new JSONResponseHandler(city);
                reader.readJsonStream(stream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Long l) {
            updateView();
        }
    }
}

