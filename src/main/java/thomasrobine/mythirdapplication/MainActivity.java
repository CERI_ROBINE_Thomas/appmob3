package thomasrobine.mythirdapplication;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    WeatherDbHelper weather;
    SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        weather = new WeatherDbHelper(MainActivity.this);
        weather.populate();
        Cursor cursor = weather.fetchAllCities();

        adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                cursor,
                new String[] {"city","country"},
                new int[] {android.R.id.text1,android.R.id.text2});

        ListView listview = (ListView) findViewById(R.id.listview);

        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Cursor curse = (Cursor) parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, CityActivity.class);
                City city = weather.cursorToCity(curse);
                intent.putExtra("Ville_informations",city);
                startActivityForResult(intent,1);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nintent = new Intent(MainActivity.this,NewCityActivity.class);
                nintent.putExtra("nville",(City) null);
                startActivityForResult(nintent,2);
            }
        });

        listview.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener()
        {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
            {
                AdapterView.AdapterContextMenuInfo mi = (AdapterView.AdapterContextMenuInfo) menuInfo;
                menu.add(0, 0, 0, "Supprimer");
            }
        });

        final SwipeRefreshLayout mySwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);

        mySwipeRefreshLayout.setOnRefreshListener(
            new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {

                    // This method performs the actual data-refresh operation.
                    // The method calls setRefreshing(false) when it's finished.
                    Cursor curse = weather.fetchAllCities();
                    if (curse.moveToFirst()) {
                        while (!curse.isAfterLast()) {
                            City c = weather.cursorToCity(curse);
                            UpdateCitiesTask uct = new UpdateCitiesTask(c);
                            uct.execute();
                            curse.moveToNext();
                        }
                    }
                    curse.close();
                    mySwipeRefreshLayout.setRefreshing(false);
                }
            }
        );

    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        AdapterView adapte = (AdapterView) findViewById(R.id.listview);
        Cursor cursor = (Cursor) adapte.getItemAtPosition(info.position);
        weather.deleteCity(cursor);
        Intent intent =new Intent(MainActivity.this,MainActivity.class);
        startActivity(intent);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            City city = data.getParcelableExtra("Ville_update");
            weather.updateCity(city);
            Cursor cursor = weather.fetchAllCities();
            adapter.changeCursor(cursor);
            adapter.notifyDataSetChanged();
            super.onActivityResult(requestCode,resultCode,data);
        }
        else if (requestCode ==2 && resultCode == Activity.RESULT_OK) {
            City city = data.getParcelableExtra("Ville_update");
            weather.addCity(city);
            Cursor cursor = weather.fetchAllCities();
            adapter.changeCursor(cursor);
            adapter.notifyDataSetChanged();
            super.onActivityResult(requestCode,resultCode,data);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class UpdateCitiesTask extends AsyncTask<URL, Integer, Long> {

        private City city;

        public UpdateCitiesTask(City c) {
            city = c;
        }

        protected Long doInBackground(URL... urls) {
            try {
                URL url = WebServiceUrl.build(city.getName(), city.getCountry());
                HttpURLConnection u = (HttpURLConnection) url.openConnection();
                InputStream stream = u.getInputStream();
                u.connect();
                JSONResponseHandler reader = new JSONResponseHandler(city);
                reader.readJsonStream(stream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(Long l) {
            weather.updateCity(city);
        }
    }
}
